import unittest
import run


class Test(unittest.TestCase):
    def test_calculate_sum(self):
        self.assertEqual(run.calculate_sum(5,6),11)
        self.assertNotEqual(run.calculate_sum(4,6),9)
    
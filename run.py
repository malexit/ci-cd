import time


def calculate_sum(a: int, b: int):
    return a + b

def main():
    if calculate_sum(3,4) == 6:
        print('str' + 'ing')
    
    next_time = time.time() + 1

    while True:
        if next_time <= time.time():
            print(next_time)
            next_time = time.time() + 1

if __name__ == '__main__':
    main()